import { injectable } from 'inversify';

import { Percentage } from '../../domain/model/unit/';
import { DrawAnalyzer } from '../../domain/model/tcg/';
import { DrawAnalyzeCommand } from './';

@injectable()
export class DrawAnalyzeUseCase {
    readonly handle = (command: DrawAnalyzeCommand): string => {
        const analyzer = new DrawAnalyzer(
            command.deckSize,
            command.drawCount,
            command.targetCardCount
        );

        const expectedValue = analyzer.expectedDraws();
        const probabilities = analyzer.aggregateProbabilitiesList();
        const result = [
            `- **デッキ枚数**: \`${command.deckSize}\` 枚`,
            `- **ドロー枚数**: \`${command.drawCount}\` 枚`,
            `- **引きたいカードの投入枚数**: \`${command.targetCardCount}\` 枚`,
            '```',
            `引きたいカードの期待値: ${expectedValue.toFixed(1)} 枚`,
        ];
        probabilities.forEach((p: Percentage, index: number) => {
            const drawCountString = index.toString()
                                   .padStart(2, ' ');
            const pString = p.toFixed(1)
                             .padStart(5);
            result.push(`${drawCountString} 枚引く確率: ${pString}${Percentage.UNIT}`);
        });
        result.push('```');

        return result.join('\n');
    }
}
