import { inject, injectable } from 'inversify';
import { ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js';
import { RESTPostAPIApplicationCommandsJSONBody } from 'discord-api-types/v10';

import { Symbols } from '../../../config/';
import { DrawAnalyzer } from '../../../domain/model/tcg/';
import { DrawAnalyzeUseCase } from '../../../usecase/tcg/';
import { ISlashCommand } from './';

@injectable()
export class TcgCommand implements ISlashCommand {
    static readonly DEFINITION = {
        name: 'tcg',
        description: 'TCG Related Commands',
        descriptionJp: 'TCG 関連のコマンド'
    }
    static readonly SUBCOMMANDS = {
        analyze: {
            name: 'analyze',
            description: 'Find the expected value or probability of drawing the desired card.',
            descriptionJp: '目的のカードを引く期待値や確率を求める。',
            options: {
                deckSize: {
                    name: 'deck-size',
                    nameJp: 'デッキ枚数',
                    description: 'Enter the number of decks',
                    descriptionJp: 'デッキ枚数を入力',
                    required: true,
                    minValue: DrawAnalyzer.MIN_DECK_SIZE
                },
                drawCount: {
                    name: 'draw-count',
                    nameJp: 'ドロー枚数',
                    description: 'Enter the count of draw',
                    descriptionJp: 'ドロー枚数を入力',
                    requiered: true,
                    minValue: DrawAnalyzer.MIN_DRAW_COUNT
                },
                targetCardCount: {
                    name: 'targetcard-count',
                    nameJp: '引きたいカードの採用枚数',
                    description: 'Enter the count of target card',
                    descriptionJp: '引きたいカードの採用枚数を入力',
                    required: true,
                    minValue: DrawAnalyzer.MIN_TARGET_CARD_COUNT
                }
            }
        }
    }

    constructor(
        @inject(Symbols.UseCase.DrawAnalyze)
        private readonly drawAnalyzeUseCase: DrawAnalyzeUseCase
    ) {}

    readonly execute = async (interaction: ChatInputCommandInteraction): Promise<void> => {
        try {
            const subcommand = interaction.options.getSubcommand();
            switch (subcommand) {
                case TcgCommand.SUBCOMMANDS.analyze.name:
                    const result = this.drawAnalyzeUseCase.handle({
                        deckSize: interaction.options.getInteger(
                            TcgCommand.SUBCOMMANDS.analyze.options.deckSize.name, true
                        ),
                        drawCount: interaction.options.getInteger(
                            TcgCommand.SUBCOMMANDS.analyze.options.drawCount.name, true
                        ),
                        targetCardCount: interaction.options.getInteger(
                            TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.name, true
                        )
                    });
                    interaction.reply({ content: result });
                    break;
                default:
                    break;
            }
        } catch (err) {
            console.error(err);
            interaction.reply({
                content: 'コマンドを実行できませんでした',
                ephemeral: true
            });
        }
    }

    readonly name = (): string => TcgCommand.DEFINITION.name;

    readonly toJSON = (): RESTPostAPIApplicationCommandsJSONBody => {
        const builder = new SlashCommandBuilder();
        builder.setName(TcgCommand.DEFINITION.name);
        builder.setDescription(TcgCommand.DEFINITION.description);
        builder.setDescriptionLocalization('ja', TcgCommand.DEFINITION.descriptionJp);

        builder.addSubcommand(subcommand => {
            subcommand.setName(TcgCommand.SUBCOMMANDS.analyze.name);
            subcommand.setDescription(TcgCommand.SUBCOMMANDS.analyze.description);
            subcommand.setDescriptionLocalization(
                'ja', TcgCommand.SUBCOMMANDS.analyze.descriptionJp
            );

            subcommand.addIntegerOption(deckSizeOption => {
                deckSizeOption.setName(TcgCommand.SUBCOMMANDS.analyze.options.deckSize.name);
                deckSizeOption.setNameLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.deckSize.nameJp
                );
                deckSizeOption.setDescription(
                    TcgCommand.SUBCOMMANDS.analyze.options.deckSize.description
                );
                deckSizeOption.setDescriptionLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.deckSize.descriptionJp
                );
                deckSizeOption.setRequired(
                    TcgCommand.SUBCOMMANDS.analyze.options.deckSize.required
                );
                deckSizeOption.setMinValue(
                    TcgCommand.SUBCOMMANDS.analyze.options.deckSize.minValue
                );
                return deckSizeOption;
            });

            subcommand.addIntegerOption(drawCountOption => {
                drawCountOption.setName(TcgCommand.SUBCOMMANDS.analyze.options.drawCount.name);
                drawCountOption.setNameLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.drawCount.nameJp
                );
                drawCountOption.setDescription(
                    TcgCommand.SUBCOMMANDS.analyze.options.drawCount.description
                );
                drawCountOption.setDescriptionLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.drawCount.descriptionJp
                );
                drawCountOption.setRequired(
                    TcgCommand.SUBCOMMANDS.analyze.options.drawCount.requiered
                );
                drawCountOption.setMinValue(
                    TcgCommand.SUBCOMMANDS.analyze.options.drawCount.minValue
                );
                return drawCountOption;
            });

            subcommand.addIntegerOption(targetCardCountOption => {
                targetCardCountOption.setName(
                    TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.name
                );
                targetCardCountOption.setNameLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.nameJp
                );
                targetCardCountOption.setDescription(
                    TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.description
                );
                targetCardCountOption.setDescriptionLocalization(
                    'ja', TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.descriptionJp
                );
                targetCardCountOption.setRequired(
                    TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.required
                );
                targetCardCountOption.setMinValue(
                    TcgCommand.SUBCOMMANDS.analyze.options.targetCardCount.minValue
                );
                return targetCardCountOption;
            });

            return subcommand;
        });

        return builder.toJSON();
    };
}
