import { inject, injectable } from 'inversify';
import { ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js';
import { RESTPostAPIApplicationCommandsJSONBody } from 'discord-api-types/v10';

import { Symbols } from '../../../config/';
import { GenerateCharacteristicsUseCase } from '../../../usecase/coc/';
import { ISlashCommand } from './';

@injectable()
export class CocCommand implements ISlashCommand {
    static readonly DEFINITION = {
        name: 'coc',
        description: 'CoC Related Commands',
        descriptionJp: 'クトゥルフ神話 TRPG 関連のコマンド',
    }
    static readonly SUBCOMMANDS = {
        characteristics: {
            name: 'generate-characteristics',
            nameJp: 'キャラクターステータス生成',
            description: 'Generate Characteristics',
            descriptionJp: 'キャラクターの能力値を生成',
            options: {
                version: {
                    name: 'version',
                    nameJp: '版',
                    description: 'Select CoC Version',
                    descriptionJp: 'CoC の版を選択',
                    required: true,
                    choices: [
                        { name: '6', value: '6' },
                        { name: '7', value: '7' }
                    ]
                }
            }
        }
    }

    constructor(
        @inject(Symbols.UseCase.GenerateCharacteristics)
        private readonly generateCharacteristicsUseCase: GenerateCharacteristicsUseCase
    ) {}

    readonly execute = async (interaction: ChatInputCommandInteraction): Promise<void> => {
        try {
            const subcommand = interaction.options.getSubcommand();
            switch (subcommand) {
                case CocCommand.SUBCOMMANDS.characteristics.name:
                    const version = interaction.options.getString(
                        CocCommand.SUBCOMMANDS.characteristics.options.version.name, true
                    );
                    const statistics = this.generateCharacteristicsUseCase.handle({
                        version: version
                    });
                    interaction.reply({ content: statistics });
                    break;
                default:
                    break;
            }
        } catch (err) {
            interaction.reply({
                content: 'CoC コマンドの実行に失敗しました',
                ephemeral: true
            });
        }
    }

    readonly name = (): string => CocCommand.DEFINITION.name;

    readonly toJSON = (): RESTPostAPIApplicationCommandsJSONBody => {
        const builder = new SlashCommandBuilder();
        builder.setName(CocCommand.DEFINITION.name);
        builder.setDescription(CocCommand.DEFINITION.description);
        builder.setDescriptionLocalization("ja", CocCommand.DEFINITION.descriptionJp);
        builder.addSubcommand(subcommand => {
            subcommand.setName(CocCommand.SUBCOMMANDS.characteristics.name);
            subcommand.setNameLocalization('ja', CocCommand.SUBCOMMANDS.characteristics.nameJp);
            subcommand.setDescription(CocCommand.SUBCOMMANDS.characteristics.description);
            subcommand.setDescriptionLocalization(
                'ja', CocCommand.SUBCOMMANDS.characteristics.descriptionJp
            );
            subcommand.addStringOption(option => {
                option.setName(CocCommand.SUBCOMMANDS.characteristics.options.version.name);
                option.setNameLocalization(
                    'ja', CocCommand.SUBCOMMANDS.characteristics.options.version.nameJp
                );
                option.setDescription(
                    CocCommand.SUBCOMMANDS.characteristics.options.version.description
                );
                option.setDescriptionLocalization(
                    'ja', CocCommand.SUBCOMMANDS.characteristics.options.version.descriptionJp
                );
                option.setRequired(
                    CocCommand.SUBCOMMANDS.characteristics.options.version.required
                );
                option.addChoices(CocCommand.SUBCOMMANDS.characteristics.options.version.choices);
                return option;
            });
            return subcommand;
        });

        return builder.toJSON()
    }
}
