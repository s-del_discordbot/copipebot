import { inject, injectable } from 'inversify';

import { Symbols } from '../../config/';
import { CharacterStatisticsFactory } from '../../domain/model/coc/statistics/';
import { assertVersionString } from '../../domain/model/coc/';
import { GenerateCharacteristicsCommand } from './';

@injectable()
export class GenerateCharacteristicsUseCase {
    constructor(
        @inject(Symbols.Factory.CharacterStatisticsFactory)
        private readonly characterStatisticsFactory: CharacterStatisticsFactory
    ) {}
    readonly handle = (command: GenerateCharacteristicsCommand): string => {
        assertVersionString(command.version);
        return this.characterStatisticsFactory.create(command.version)
                                              .toMarkdownString()
    }
}
