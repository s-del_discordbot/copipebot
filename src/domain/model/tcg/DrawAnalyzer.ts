import { combinations as C } from 'mathjs';

import { Percentage } from '../unit/';

/**
 * 引数に与えられた以下の要素から、期待値や確率を求めるためのクラス。
 * - デッキ枚数
 * - ドロー枚数
 * - 目的のカードの投入枚数
 */
export class DrawAnalyzer {
    static readonly MIN_DECK_SIZE = 1;
    static readonly MIN_DRAW_COUNT = 1;
    static readonly MIN_TARGET_CARD_COUNT = 1;

    constructor(
        private readonly deckSize: number,
        private readonly drawCount: number,
        private readonly targetCardCount: number
    ) {
        if (this.deckSize < DrawAnalyzer.MIN_DECK_SIZE) {
            throw new Error(`デッキ枚数の指定が ${DrawAnalyzer.MIN_DECK_SIZE} 未満`);
        }
        if (this.drawCount < DrawAnalyzer.MIN_DRAW_COUNT) {
            throw new Error(`ドロー枚数の指定が ${DrawAnalyzer.MIN_DRAW_COUNT} 未満`);
        }
        if (this.targetCardCount < DrawAnalyzer.MIN_TARGET_CARD_COUNT) {
            throw new Error(`目的のカード枚数の指定が ${DrawAnalyzer.MIN_TARGET_CARD_COUNT} 未満`);
        }
        if (this.drawCount > this.deckSize) {
            throw new Error(`ドロー枚数がデッキ枚数より多い`);
        }
        if (this.targetCardCount > this.deckSize) {
            throw new Error(`目的のカード枚数がデッキ枚数より多い`);
        }
    }

    /**
     * 目的のカードが何枚引けるかの期待値を返す
     *
     * 目的のカードが n 枚採用されている d 枚のデッキから、
     * x 枚のカードを引いた時、
     * その中に目的のカードが何枚含まれることを期待できるか (期待値 e) を求める式は以下となる。
     *
     * `e = n / d * x`
     *
     * @return {number} 期待値 e を表す数値
     */
    readonly expectedDraws = (): number => {
        const n = this.targetCardCount;
        const d = this.deckSize;
        const x = this.drawCount;

        return n / d * x;
    }

    /**
     * 目的のカードを指定した枚数引ける確率を返す
     *
     * d 枚のデッキから h 枚のカードを引いた時、
     * n 枚採用しているカードが x 枚引ける確率 p を求めるのは以下の式となる。
     *
     * `p = nCx * (d-n)C(h-x) / dCh * 100`
     *
     * @param {number} x - 目的のカードを引きたい枚数
     * @return {Percentage} x で指定された枚数を引く確率
     */
    readonly targetCardDrawsProbability = (x: number): Percentage => {
        const n = this.targetCardCount;
        const d = this.deckSize;
        const h = this.drawCount;

        return new Percentage(C(n, x) * C(d-n, h-x) / C(d, h) * 100)
    }

    /**
     * 引く確率をまとめたリストを返す。
     *
     * リストのインデックスが枚数、要素がその枚数引く確率を表す。
     *
     * 返されるリストの最大長は、以下のいずれかの大きい方となる。
     * - 引くカードの数
     * - 採用枚数の数
     *
     * @returns {Percentage[]} 各枚数を引く確率が格納されたリスト
     */
    readonly aggregateProbabilitiesList = (): Percentage[] => {
        const probabilities = [];
        for (let x = 0; x <= this.drawCount; x++) {
            if (x > this.targetCardCount) break;
            probabilities.push(this.targetCardDrawsProbability(x));
        }

        return probabilities;
    }
}
