export type Version = '6' | '7';

export function assertVersionString(str: string): asserts str is Version {
    if (str !== '6' && str !== '7') {
        throw new Error('Version の指定が不正');
    }
}
