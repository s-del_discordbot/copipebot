import { IValueObject } from '../../';
import { StatusName } from './';

export class Status implements IValueObject<number> {
    constructor(
        private readonly name: StatusName,
        private readonly value: number
    ) {}

    readonly equals = (other: unknown): boolean => {
        return other instanceof Status
               && other.name === this.name
               && other.value === this.value;
    }

    readonly nameOf = (): StatusName => this.name;

    readonly toString = (): string => `${this.name}: ${this.value}`;

    readonly toMarkdownString = (): string => `- **${this.name}**: \`${this.value}\``;

    readonly valueOf = (): number => this.value;
}
