import { Status } from './';

export class CharacterStatistics {
    constructor(
        private readonly str: Status,
        private readonly con: Status,
        private readonly siz: Status,
        private readonly dex: Status,
        private readonly app: Status,
        private readonly int: Status,
        private readonly pow: Status,
        private readonly edu: Status
    ) {
        if (this.str.nameOf() != 'STR') throw new Error(`STR の名前が不正 ${this.str.nameOf()}`);
        if (this.con.nameOf() != 'CON') throw new Error(`CON の名前が不正 ${this.str.nameOf()}`);
        if (this.siz.nameOf() != 'SIZ') throw new Error(`SIZ の名前が不正 ${this.str.nameOf()}`);
        if (this.dex.nameOf() != 'DEX') throw new Error(`DEX の名前が不正 ${this.str.nameOf()}`);
        if (this.app.nameOf() != 'APP') throw new Error(`APP の名前が不正 ${this.str.nameOf()}`);
        if (this.int.nameOf() != 'INT') throw new Error(`INT の名前が不正 ${this.str.nameOf()}`);
        if (this.pow.nameOf() != 'POW') throw new Error(`POW の名前が不正 ${this.str.nameOf()}`);
        if (this.edu.nameOf() != 'EDU') throw new Error(`EDU の名前が不正 ${this.str.nameOf()}`);
    }

    readonly toString = (): string => {
        return [
            `${this.str}`,
            `${this.con}`,
            `${this.siz}`,
            `${this.dex}`,
            `${this.app}`,
            `${this.int}`,
            `${this.pow}`,
            `${this.edu}`
        ].join('\n');
    }

    readonly toMarkdownString = (): string => {
        return [
            `${this.str.toMarkdownString()}`,
            `${this.con.toMarkdownString()}`,
            `${this.siz.toMarkdownString()}`,
            `${this.dex.toMarkdownString()}`,
            `${this.app.toMarkdownString()}`,
            `${this.int.toMarkdownString()}`,
            `${this.pow.toMarkdownString()}`,
            `${this.edu.toMarkdownString()}`
        ].join('\n');
    }
}
