import { injectable } from 'inversify';

import { Dice, NumberOfSurface, RollAmount } from '../../dice/';
import { Version } from '../';
import { CharacterStatistics, Status, StatusName } from './';

@injectable()
export class CharacterStatisticsFactory {
    readonly str = (version: Version): Status => {
        const name: StatusName = 'STR';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(3))
                                                      .total();
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly con = (version: Version): Status => {
        const name: StatusName = 'CON';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(3))
                                                      .total();
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly siz = (version: Version): Status => {
        const name: StatusName = 'SIZ';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(2))
                                                      .total() + 6;
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly dex = (version: Version): Status => {
        const name: StatusName = 'DEX';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(3))
                                                      .total();
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly app = (version: Version): Status => {
        const name: StatusName = 'APP';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(3))
                                                      .total();
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly int = (version: Version): Status => {
        const name: StatusName = 'INT';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(2))
                                                      .total() + 6;
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly pow = (version: Version): Status => {
        const name: StatusName = 'POW';
        const total = new Dice(new NumberOfSurface(6)).roll(new RollAmount(3))
                                                      .total();
        return version === '6' ? new Status(name, total)
                               : new Status(name, total * 5);
    }

    readonly edu = (version: Version): Status => {
        const name: StatusName = 'EDU';
        const rollAmount = version === '6' ? new RollAmount(3)
                                           : new RollAmount(2);
        const total = new Dice(new NumberOfSurface(6)).roll(rollAmount)
                                                      .total();
        return version === '6' ? new Status(name, total + 3)
                               : new Status(name, (total+6) * 5);
    }

    readonly create = (version: Version): CharacterStatistics => {
        return new CharacterStatistics(
            this.str(version),
            this.con(version),
            this.siz(version),
            this.dex(version),
            this.app(version),
            this.int(version),
            this.pow(version),
            this.edu(version)
        );
    }
}
