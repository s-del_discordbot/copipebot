import { IValueObject } from '../';

export class Percentage implements IValueObject<number> {
    static readonly UNIT = '%';

    constructor(private readonly value: number) {}

    readonly equals = (other: unknown): boolean => {
        return other instanceof Percentage
               && other.value === this.value;
    }

    readonly toFixed = (fractionDigits?: number): string => {
        return this.value.toFixed(fractionDigits);
    }

    readonly toString = (fractionDigits?: number): string => {
        return `${this.toFixed(fractionDigits)}${Percentage.UNIT}`;
    }

    readonly valueOf = (): number => this.value;

    readonly toMultiplier = (): number => this.value / 100;
}
